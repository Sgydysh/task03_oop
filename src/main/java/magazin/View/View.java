package magazin.View;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import magazin.controller.*;


public class View {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public View() {
        controller = new Arhcontroller();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - показати товар ");
        menu.put("2", "  2 - додати товар ");
        menu.put("3", "  3 - додати і показати  ");
        menu.put("4", "  4 - змінити ");
        menu.put("5", " видалити");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

  private void pressButton1() {
        controller.showTowar();
    }

    private void pressButton2() {
        controller.add();
    }

    private void pressButton3() {
        controller.add();
        controller.showTowar();
    }

    private void pressButton4() {
        controller.change();
    }

    private void pressButton5() {
        controller.delete();
    }
//private void pressButton6(){ model.getKilkisty();}
    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
