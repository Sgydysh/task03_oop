package magazin.View;

@FunctionalInterface
public interface Printable {

    void print();
}
