package magazin.Model;

import java.util.List;

public interface Model {
    List<Prylad> getTowar();
/**    додавання в ArrayList */
    void aDD();
/**  перевірка наявності елементу */
    void isElement(String name);
/**  видалення елементу за назвою*/
    void delete(String name);
/** зміна за назвою */
    void change(String name);
/**  показ всіх товарів наявниих  Array List */
    void showTowar();
}