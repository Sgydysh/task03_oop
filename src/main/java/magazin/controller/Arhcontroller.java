package magazin.controller;

import magazin.Model.Core;
import magazin.Model.Model;

import java.util.Scanner;
/** класс імплементує всі методи інтерфеусу Controller  та оверрайдит їх потім цей класс буде створюватися в класі View  */
public class Arhcontroller implements Controller {
    Model model;
/** конструктор  */
    public Arhcontroller() {

        model = new Core();
    }
    /** перезапис функції */
    @Override
    public void showTowar() {
        model.showTowar();
    }

    @Override
    public void add() {
        model.aDD();
    }

    @Override
    public void isElement() {
        System.out.println("Введіть назву товару ");
        model.isElement(new Scanner(System.in).nextLine());
    }

    @Override
    public void delete() {
        System.out.println("Введіть назву товару ");
        model.delete(new Scanner(System.in).nextLine());
    }

    @Override
    public void change() {
        System.out.println("Введіть назву товару ");
        model.change(new Scanner(System.in).nextLine());
    }
}
