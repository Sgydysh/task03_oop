package magazin.controller;

/**
 * інтерфейс , який буде кспадкований Archcontroller
 */
public interface Controller {
    /**
     * створення функція, яка показує всі товари, шляхом виклику іншої, яка описана в інтерфейсі Model
     */
    void showTowar();

    /**
     * функція яка додає новий елемент до нашого ArrayList повна реалізаціія описана в Model
     */
    void add();
/**  функція яка провіряє наявність елементу   */
    void isElement();
/**  функція яка видаляє елемент */
    void delete();
/**  функція яка змінює елемент  */
    void change();


}
